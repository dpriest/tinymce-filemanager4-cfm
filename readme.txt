Set up:
1. Copy tinymce\plugins\filemanager folder into tinymce\plugins\ folder in your site.

2. Open tinymce\plugins\filemanager\settings.cfm and provide the absolute path and URL to your storage folder. 
   Example : Absolute path would be : C:\interpub\wwwroot\mysite.com\userFiles
   and URL would be : http://www.mysite.com/userFiles
   If you want to store files in a multi-user environment, you can programmatically change this location (such as C:\interpub\wwwroot\mysite.com\userFiles\#session.users_id#)

3. Add filemanager plugin to your TinyMCE 'toolbar' and 'plugins' setting arguments. Refer to editor.cfm file for an example.

4. Optional: FileManager need to know the path to itself from the page you are calling it. It tries to find this path automatically. 
   But if there is any issue you can still provide this path manually as a setting argument, fileManager_path, in TinyMCE.
   Example: fileManager_path : "js/TinyMCE/plugins/FileManager/index.cfm"

5. Optional: FileManager use jQuery and it been loaded from jQuery-CDN into filemanager/index.cfm file. If you want to include
   your own copy, please provide in there.

**************************************************************************
Important: Add your own user authentication at the top of filemanager.cfc, upload.cfm and folder.cfm - if you do not authenticate access to these pages, anyone will be able to edit/delete/update files. 
**************************************************************************

Please content me at twitter : cfloveorg, if you have any issues. I will try my best to help you out.