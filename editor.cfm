<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tiny</title>
	<script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
</head>
<body>

<script type="text/javascript" src="tinymce/tinymce.jquery.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    toolbar: "filemanager undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    plugins: [
        "filemanager advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ]
    //,fileManager_path : "TinyMCE/plugins/FileManager/index.cfm"
});
</script>

<!-- Place this in the body of the page content -->
<form method="post">
    <textarea></textarea>
</form>

</body>
</html>